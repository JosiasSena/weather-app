package josias.simple_weather.util.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import josias.simple_weather.objects.Location;

public class MyLocationsDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "places.sqlite";
    private static final int VERSION = 2;
    private static final String TABLE_PLACES = "places";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_LAT_LONG = "lat_long";
    private final String[] allLocationColumns = {COLUMN_ID, COLUMN_NAME, COLUMN_LAT_LONG};

    public MyLocationsDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_PLACES + "(" + COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_NAME + " text not null, " +
                COLUMN_LAT_LONG + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertNewLocation(Location location) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, location.getName());
        cv.put(COLUMN_LAT_LONG, location.getLatLong());

        long insertId = getWritableDatabase().insert(TABLE_PLACES, null, cv);
        Cursor cursor = getWritableDatabase().query(TABLE_PLACES, allLocationColumns, COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
    }

    public void deleteLocation(Location location) {
        long id = location.getId();
        getWritableDatabase().delete(TABLE_PLACES, COLUMN_ID + " = " + id, null);
    }

    /**
     * @return Returns all locations in the database
     */
    public List<Location> getAllLocations() {
        List<Location> locations = new ArrayList<>();

        Cursor cursor = getWritableDatabase().query(
                TABLE_PLACES,
                allLocationColumns,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Location location = cursorToLocation(cursor);
            locations.add(location);
            cursor.moveToNext();
        }
        cursor.close();
        return locations;
    }

    /**
     * Convert a cursor to a location
     * @param cursor
     * @return
     */
    private Location cursorToLocation(Cursor cursor) {
        Location location = new Location();
        location.setId(cursor.getLong(0)); // get cursor id
        location.setName(cursor.getString(1)); // get cursor name
        location.setLatLong(cursor.getString(2)); // get cursor latlong
        return location;
    }
}

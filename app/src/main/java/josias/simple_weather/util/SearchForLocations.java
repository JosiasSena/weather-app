package josias.simple_weather.util;

import android.content.Context;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.widget.SimpleCursorAdapter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;

import josias.simple_weather.activities.MyLocationsListActivityDialog;
import josias.simple_weather.R;

import static josias.simple_weather.util.Constants.TAG_CITY;
import static josias.simple_weather.util.Constants.TAG_COORDINATES;
import static josias.simple_weather.util.Constants.TAG_NAME;
import static josias.simple_weather.util.Constants.TAG_TYPE;

/**
 * Searches Weather Underground API for all locations
 * starting with the query parameters
 */
public class SearchForLocations extends AsyncTask<Void, Void, Void> {
    private static final String TAG_RESULT = "RESULTS";
    private static final String TAG_LAT = "lat";
    private static final String TAG_LON = "lon";
    private static final String TAG_ID = "_id";

    private final String query;
    private MatrixCursor cursor;
    private final Context context;

    public SearchForLocations(Context context, String query) {
        this.context = context;
        this.query = query;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            cursor = new MatrixCursor(new String[]{TAG_ID, TAG_NAME, TAG_COORDINATES});
            String url = "http://autocomplete.wunderground.com/aq?query=" + query;
            JsonNode rootNode = mapper.readTree(new URL(url));
            JsonNode results = rootNode.get(TAG_RESULT);

            for (int i = 0; i < results.size(); i++) {
                JsonNode mLocation = results.get(i);

                // Only show cities
                if (mLocation.findValue(TAG_TYPE).textValue().equals(TAG_CITY)) {
                    String name = mLocation.findValue(TAG_NAME).textValue();
                    String lat = mLocation.findValue(TAG_LAT).textValue();
                    String lon = mLocation.findValue(TAG_LON).textValue();

                    String coordinates = lat + "," + lon;

                    Object[] myObjects = new Object[3];
                    myObjects[0] = i; // id
                    myObjects[1] = name; // name
                    myObjects[2] = coordinates; //latitude and longitude
                    cursor.addRow(myObjects);
                    cursor.moveToNext();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        //noinspection deprecation
        MyLocationsListActivityDialog.searchView
                .setSuggestionsAdapter(new SimpleCursorAdapter(
                        context,
                        R.layout.simple_list_item,
                        cursor,
                        new String[]{TAG_NAME},
                        new int[]{android.R.id.text1}));
    }
}
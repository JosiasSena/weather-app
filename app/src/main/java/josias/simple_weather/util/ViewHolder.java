package josias.simple_weather.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import josias.simple_weather.R;

/**
 * Very simple viewHolder class to hold item views;
 */
public class ViewHolder extends RecyclerView.ViewHolder {
    public final ImageView mWeatherIcon;
    public final TextView hourTime;
    public final TextView hourlyTemp;
    public final TextView hourlyFeelsLike;
    public final TextView hourlyCondition;
    public final TextView hiTemp;
    public final TextView loTemp;
    public final TextView dayName;

    public ViewHolder(View view) {
        super(view);
        mWeatherIcon = (ImageView) view.findViewById(R.id.mWeatherIcon);
        hourTime = (TextView) view.findViewById(R.id.hourTime);
        hourlyTemp = (TextView) view.findViewById(R.id.hourlyTemp);
        hourlyFeelsLike = (TextView) view.findViewById(R.id.hourlyFeelsLike);
        hourlyCondition = (TextView) view.findViewById(R.id.hourlyCondition);
        dayName = (TextView) view.findViewById(R.id.dayName);
        hiTemp = (TextView) view.findViewById(R.id.hiTemp);
        loTemp = (TextView) view.findViewById(R.id.loTemp);
    }
}
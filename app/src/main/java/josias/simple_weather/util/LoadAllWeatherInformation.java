package josias.simple_weather.util;

import android.app.Activity;
import android.app.Notification;
import android.os.Build;

import com.squareup.picasso.Picasso;

import josias.simple_weather.adapters.MyRecyclerAdapter;
import josias.simple_weather.fragments.ForeCastInformationFragment;
import josias.simple_weather.fragments.HourlyInformationFragment;
import josias.simple_weather.weather.GetForecastConditions;
import josias.simple_weather.weather.GetHourlyConditions;

import static josias.simple_weather.activities.MainActivity.enableNotifications;
import static josias.simple_weather.activities.MainActivity.forecastInformationArrayList;
import static josias.simple_weather.activities.MainActivity.hourlyInformationArrayList;
import static josias.simple_weather.activities.MainActivity.nowInfo;
import static josias.simple_weather.activities.MainActivity.onGoingNotification;
import static josias.simple_weather.fragments.CurrentInformationFragment.conditionTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.dewpointTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.feelsLikeTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.humidityTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.locationTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.tempTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.todaysWeatherImageView;
import static josias.simple_weather.fragments.CurrentInformationFragment.windDirTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.windSpeedTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.windchillTV;
import static josias.simple_weather.util.Constants.FORECAST;
import static josias.simple_weather.util.Constants.HOURLY;
import static josias.simple_weather.util.Constants.TAG_DEW_POINT;
import static josias.simple_weather.util.Constants.TAG_FEELSLIKE;
import static josias.simple_weather.util.Constants.TAG_HOURLY_TEMP;
import static josias.simple_weather.util.Constants.TAG_HUMIDITY;
import static josias.simple_weather.util.Constants.TAG_ICON_URL;
import static josias.simple_weather.util.Constants.TAG_LOCATION;
import static josias.simple_weather.util.Constants.TAG_WEATHER;
import static josias.simple_weather.util.Constants.TAG_WIND_CHILL;
import static josias.simple_weather.util.Constants.TAG_WIND_DIRECTION;
import static josias.simple_weather.util.Constants.TAG_WIND_SPEED;

public class LoadAllWeatherInformation {
    private Activity activity;
    private GetHourlyConditions getHourlyConditions;
    private GetForecastConditions getForecastConditions;

    public LoadAllWeatherInformation(Activity activity, GetHourlyConditions getHourlyConditions, GetForecastConditions getForecastConditions) {
        this.activity = activity;
        this.getHourlyConditions = getHourlyConditions;
        this.getForecastConditions = getForecastConditions;
    }

    public void load() {
        if (nowInfo == null || nowInfo.size() <= 0 ||
                hourlyInformationArrayList == null || hourlyInformationArrayList.size() <= 0 ||
                forecastInformationArrayList == null || forecastInformationArrayList.size() <= 0) {
            getHourlyConditions.execute();
            getForecastConditions.execute();
        } else {
            Picasso.with(activity.getApplicationContext()).load(nowInfo.get(TAG_ICON_URL)).fit().noFade().into(todaysWeatherImageView);
            locationTV.setText(nowInfo.get(TAG_LOCATION));
            tempTV.setText(nowInfo.get(TAG_HOURLY_TEMP));
            conditionTV.setText(nowInfo.get(TAG_WEATHER));
            humidityTV.setText(nowInfo.get(TAG_HUMIDITY));
            windDirTV.setText(nowInfo.get(TAG_WIND_DIRECTION));
            feelsLikeTV.setText(nowInfo.get(TAG_FEELSLIKE));
            windSpeedTV.setText(nowInfo.get(TAG_WIND_SPEED));
            windchillTV.setText(nowInfo.get(TAG_WIND_CHILL));
            dewpointTV.setText(nowInfo.get(TAG_DEW_POINT));

            MyRecyclerAdapter hourlyAdapter = new MyRecyclerAdapter(activity, hourlyInformationArrayList, HOURLY);
            HourlyInformationFragment.mRecyclerView.setAdapter(hourlyAdapter);

            MyRecyclerAdapter forecastAdapter = new MyRecyclerAdapter(activity, forecastInformationArrayList, FORECAST);
            ForeCastInformationFragment.mRecyclerView.setAdapter(forecastAdapter);

            WeatherNotification weatherNotification = new WeatherNotification(activity, onGoingNotification);
            weatherNotification.display();
            weatherNotification.enableNotification(enableNotifications);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // if build is Lollipop or higher then set notification visibility to public
                weatherNotification.setVisibility(Notification.VISIBILITY_PUBLIC);
            }
        }
    }
}

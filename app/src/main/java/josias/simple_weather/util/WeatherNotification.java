package josias.simple_weather.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.net.URL;

import josias.simple_weather.R;
import josias.simple_weather.activities.MainActivity;

import static josias.simple_weather.activities.MainActivity.nowInfo;
import static josias.simple_weather.util.Constants.TAG_HOURLY_TEMP;
import static josias.simple_weather.util.Constants.TAG_ICON_URL;
import static josias.simple_weather.util.Constants.TAG_LOCATION;
import static josias.simple_weather.util.Constants.TAG_WEATHER;

public class WeatherNotification {
    private Context context;
    private boolean onGoing;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    public WeatherNotification(Context context, boolean onGoing) {
        this.context = context;
        this.onGoing = onGoing;
    }

    public void display() {
        // Create pending intent to set which activity will be opened on notification click
        Intent mResultIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, mResultIntent, 0);

        mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.weather) // status bar icon
                .setContentTitle(nowInfo.get(TAG_HOURLY_TEMP)
                        .substring(0, nowInfo.get(TAG_HOURLY_TEMP).length() - 3) +
                        "°" + // alt + 0176
                        " - " + nowInfo.get(TAG_WEATHER)) // Weather condition
                .setContentText(nowInfo.get(TAG_LOCATION)) // location
                .setContentInfo("JSG") // Josias Sena Gomez = Carrot Creative
                .setContentIntent(pendingIntent) // intent to open when notification gets clicked
                .setOngoing(onGoing); // cannot be removed

        try {
            URL url = new URL(nowInfo.get(TAG_ICON_URL));
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            mBuilder.setLargeIcon(image); // the current conditions icon
        } catch (IOException e) {
            e.printStackTrace();
        }

        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build()); // publish notification
    }

    public void setVisibility(int visibility) {
        mBuilder.setVisibility(visibility);
    }

    public void enableNotification(boolean enabled) {
        if (enabled) {
            mNotificationManager.notify(0, mBuilder.build());
        } else {
            mNotificationManager.cancelAll();
        }
    }
}

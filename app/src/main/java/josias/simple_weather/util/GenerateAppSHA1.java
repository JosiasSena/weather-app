package josias.simple_weather.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * This class generates the SHA1 for Google API usage. eg. Location.
 * This can be done through the terminal as well but this way we don't have to worry about keys
 */
public class GenerateAppSHA1 {
    private final Context context;
    private PackageInfo packageInfo;
    private CertificateFactory certificateFactory;
    private X509Certificate x509Certificate;
    private String hexString;

    public GenerateAppSHA1(Context context) {
        this.context = context;
    }

    public String getCertificateSHA1Fingerprint() {
        getPackageInfo();
        getCertificateFactory();
        getX509Cert();
        getMessageDigest();
        return hexString;
    }

    private void getMessageDigest() {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            byte[] publicKey = messageDigest.digest(x509Certificate != null ? x509Certificate.getEncoded() : new byte[0]);
            hexString = byte2HexFormatted(publicKey);
        } catch (NoSuchAlgorithmException | CertificateEncodingException e1) {
            e1.printStackTrace();
        }
    }

    private void getX509Cert() {
        Signature[] signatures = packageInfo != null ? packageInfo.signatures : new Signature[0];
        byte[] cert = signatures[0].toByteArray();
        InputStream input = new ByteArrayInputStream(cert);

        try {
            x509Certificate = (X509Certificate) (certificateFactory != null ? certificateFactory.generateCertificate(input) : null);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    private void getCertificateFactory() {
        try {
            certificateFactory = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    private void getPackageInfo() {
        try {
            PackageManager packageManager = context.getPackageManager();
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static String byte2HexFormatted(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++) {
            String hexString = Integer.toHexString(bytes[i]);

            int length = hexString.length();

            if (length == 1) {
                hexString = "0" + hexString;
            } else if (length > 2) {
                hexString = hexString.substring(length - 2, length);
            }

            stringBuilder.append(hexString.toUpperCase());

            if (i < (bytes.length - 1)) {
                stringBuilder.append(':');
            }
        }
        return stringBuilder.toString();
    }
}

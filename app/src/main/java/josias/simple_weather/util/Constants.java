package josias.simple_weather.util;

import android.appwidget.AppWidgetManager;

public class Constants {
    /**
     * Here are different API keys, when one has exceeded limit for the day just use another one
     *  - 77173c946cbbf695
     *  - a8840032673228dd
     *  - 0cc97be749e77792
     */

    public static final String WEATHER_ID = "0cc97be749e77792";

    public static final String ACTION_APPWIDGET_UPDATE = AppWidgetManager.ACTION_APPWIDGET_UPDATE;
    public static final String TAG_FEELSLIKE = "feelslike";
    public static final String TAG_HOURLY_TEMP = "temp";
    public static final String TAG_CONDITION = "condition";
    public static final String TAG_CONDITIONS = "conditions";
    public static final String TAG_TIME = "civil";
    public static final String TAG_ICON_URL = "icon_url";
    public static final String TAG_HUMIDITY = "humidity";
    public static final String TAG_DEW_POINT = "dewpoint";
    public static final String TAG_WIND_SPEED = "wspd";
    public static final String TAG_WIND_CHILL = "windchill";
    public static final String TAG_WIND_DIRECTION = "wdir";
    public static final String HOUR_KEY = "my_hour";
    public static final String mLAT_LONG = "latLong";
    public static final String TAG_FORECAST = "forecast";
    public static final String TAG_TEN_DAY_FORECAST = "forecast10day";
    public static final String TAG_DAY_NAME = "weekday";
    public static final String TAG_HIGH = "high";
    public static final String TAG_LOW = "low";
    public static final String TAG_COORDINATES = "coordinates";
    public static final String BAD_COORDINATES = "-9999.000000,-9999.000000";
    public static final String TAG_NAME = "name";
    public static final String TAG_LOCATION = "location";
    public static final String TAG_TYPE = "type";
    public static final String TAG_CITY = "city";
    public static final String TAG_FCTEXT = "fcttext";

    public static final String TAG_DISPLAY_LOCATION = "display_location";
    public static final String TAG_CURRENT_OBSERVATION = "current_observation";
    public static final String TAG_ZIP = "zip";
    public static final String TAG_FULL = "full";
    public static final String TAG_TEMP = "temperature_string";
    public static final String TAG_TEMP_F = "temp_f";
    public static final String TAG_TEMP_C = "temp_c";
    public static final String TAG_WEATHER = "weather";
    public static final String TAG_LOCALTIME = "local_time_rfc822";
    public static final String TAG_HOURLYROOT = "hourly_forecast";
    public static final String TAG_FCTTIME = "FCTTIME";
    public static final String TAG_ENGLISH = "english";
    public static final String TAG_MDAY = "mday";
    public static final String TAG_RELATIVE_HUMIDITY = "relative_humidity";

    public static final String TAG_WIND_DIR = "wind_dir";
    public static final String TAG_WIND_SPEED_MPH = "wind_mph";
    public static final String TAG_DEW_POINT_F = "dewpoint_f";
    public static final String TAG_WIND_CHILL_F = "windchill_f";
    public static final String TAG_FEELS_LIKE_F = "feelslike_f";

    public static final int HOURLY = 0;
    public static final int FORECAST = 1;

    // Request code to use when launching the resolution activity
    public static final int REQUEST_RESOLVE_ERROR = 1001;
}

package josias.simple_weather.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class Hour implements Parcelable {

    private String TIME;
    private String TEMP;
    private String CONDITION;
    private String HUMIDITY;
    private String DEW_POINT;
    private String WIND_SPEED;
    private String WIND_CHILL;
    private String WIND_DIRECTION;
    private String ICON_URL;
    private String FEELS_LIKE;

    public String getTIME() {
        return TIME;
    }

    public void setTIME(String TIME) {
        this.TIME = TIME;
    }

    public String getTEMP() {
        return TEMP;
    }

    public void setTEMP(String TEMP) {
        this.TEMP = TEMP;
    }

    public String getCONDITION() {
        return CONDITION;
    }

    public void setCONDITION(String CONDITION) {
        this.CONDITION = CONDITION;
    }

    public String getHUMIDITY() {
        return HUMIDITY;
    }

    public void setHUMIDITY(String HUMIDITY) {
        this.HUMIDITY = HUMIDITY;
    }

    public String getDEW_POINT() {
        return DEW_POINT;
    }

    public void setDEW_POINT(String DEW_POINT) {
        this.DEW_POINT = DEW_POINT;
    }

    public String getWIND_SPEED() {
        return WIND_SPEED;
    }

    public void setWIND_SPEED(String WIND_SPEED) {
        this.WIND_SPEED = WIND_SPEED;
    }

    public String getWIND_CHILL() {
        return WIND_CHILL;
    }

    public void setWIND_CHILL(String WIND_CHILL) {
        this.WIND_CHILL = WIND_CHILL;
    }

    public String getWIND_DIRECTION() {
        return WIND_DIRECTION;
    }

    public void setWIND_DIRECTION(String WIND_DIRECTION) {
        this.WIND_DIRECTION = WIND_DIRECTION;
    }

    public String getICON_URL() {
        return ICON_URL;
    }

    public void setICON_URL(String ICON_URL) {
        this.ICON_URL = ICON_URL;
    }

    public String getFEELS_LIKE() {
        return FEELS_LIKE;
    }

    public void setFEELS_LIKE(String FEELS_LIKE) {
        this.FEELS_LIKE = FEELS_LIKE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Hour> CREATOR = new Creator<Hour>() {
        public Hour createFromParcel(Parcel source) {
            Hour mHour = new Hour();
            mHour.TIME = source.readString();
            mHour.TEMP = source.readString();
            mHour.CONDITION = source.readString();
            mHour.HUMIDITY = source.readString();
            mHour.DEW_POINT = source.readString();
            mHour.WIND_SPEED = source.readString();
            mHour.WIND_CHILL = source.readString();
            mHour.WIND_DIRECTION = source.readString();
            mHour.ICON_URL = source.readString();
            mHour.FEELS_LIKE = source.readString();
            return mHour;
        }

        @Override
        public Hour[] newArray(int size) {
            return new Hour[0];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(TIME);
        parcel.writeString(TEMP);
        parcel.writeString(CONDITION);
        parcel.writeString(HUMIDITY);
        parcel.writeString(DEW_POINT);
        parcel.writeString(WIND_SPEED);
        parcel.writeString(WIND_CHILL);
        parcel.writeString(WIND_DIRECTION);
        parcel.writeString(ICON_URL);
        parcel.writeString(FEELS_LIKE);
    }
}

package josias.simple_weather.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import josias.simple_weather.R;

public class CurrentInformationFragment extends Fragment {

    public static ImageView todaysWeatherImageView;
    public static TextView conditionTV, tempTV, locationTV, humidityTV,
            windDirTV, feelsLikeTV, windSpeedTV, windchillTV, dewpointTV;

    public CurrentInformationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_current_information, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        todaysWeatherImageView = (ImageView) rootView.findViewById(R.id.todaysWeatherImageView);

        conditionTV = (TextView) rootView.findViewById(R.id.conditionTV);
        tempTV = (TextView) rootView.findViewById(R.id.tempTV);
        locationTV = (TextView) rootView.findViewById(R.id.locationTV);
        humidityTV = (TextView) rootView.findViewById(R.id.humidityTV);
        windDirTV = (TextView) rootView.findViewById(R.id.windDirTV);
        feelsLikeTV = (TextView) rootView.findViewById(R.id.feelsLikeTV);
        windSpeedTV = (TextView) rootView.findViewById(R.id.windSpeedTV);
        windchillTV = (TextView) rootView.findViewById(R.id.windchillTV);
        dewpointTV = (TextView) rootView.findViewById(R.id.dewpointTV);
    }
}

package josias.simple_weather.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import josias.simple_weather.R;

public class ForeCastInformationFragment extends Fragment {
    public static RecyclerView mRecyclerView;

    public ForeCastInformationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_weather_information_layout, container, false);
        initRecyclerView(rootView);
        return rootView;
    }

    /**
     * Initialize RecyclerView and sets its layoutManager
     */
    private void initRecyclerView(View rootView) {
        int orientation = LinearLayoutManager.VERTICAL;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        layoutManager.setOrientation(orientation);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);
    }
}

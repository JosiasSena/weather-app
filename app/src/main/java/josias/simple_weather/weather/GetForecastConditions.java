package josias.simple_weather.weather;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import josias.simple_weather.R;
import josias.simple_weather.adapters.MyRecyclerAdapter;
import josias.simple_weather.fragments.ForeCastInformationFragment;

import static josias.simple_weather.activities.MainActivity.forecastInformationArrayList;
import static josias.simple_weather.util.Constants.*;

public class GetForecastConditions extends AsyncTask<Void, Void, Void> {

    private static final String mDATE = "date";
    private static final String mTITLE = "title";
    private static final String mWEEKDAY = "weekday";
    private static final String mFAHR = "fahrenheit";
    private static final String mSIMPLE_FORECAST = "simpleforecast";
    private static final String TAG_TXT_FORECAST = "txt_forecast";
    private static final String mFORECAST_DAY = "forecastday";
    private static final String LOG = GetForecastConditions.class.getSimpleName();

    private String FORECAST_CONDITIONS_URL;
    private final Activity activity;

    public GetForecastConditions(Activity activity, String FORECAST_CONDITIONS_URL) {
        this.activity = activity;
        setForeCastUrl(FORECAST_CONDITIONS_URL);
    }

    @Override
    protected Void doInBackground(Void... params) {
        ObjectMapper forecastMapper = new ObjectMapper();
        forecastInformationArrayList = new ArrayList<>();

        try {
            Log.d("FORECAST_CONDITIONS_URL", FORECAST_CONDITIONS_URL);
            JsonNode forecastRootNode = forecastMapper.readTree(new URL(FORECAST_CONDITIONS_URL));
            JsonNode dailyRoot = forecastRootNode.get(TAG_FORECAST);

            if (dailyRoot == null) {
                Log.e(LOG, activity.getString(R.string.null_root) + " --> " + activity.getString(R.string.exeeding_plan));
            } else {
                JsonNode textForecast = dailyRoot.get(TAG_TXT_FORECAST);
                JsonNode txtForecastDay = textForecast.get(mFORECAST_DAY);

                JsonNode simpleForecast = dailyRoot.get(mSIMPLE_FORECAST);
                JsonNode forecastDay = simpleForecast.get(mFORECAST_DAY);

                for (int i = 0; i < forecastDay.size(); i++) {
                    JsonNode day = forecastDay.get(i);
//                    JsonNode txt_day = txtForecastDay.get(i);

//                    String forecast_text = txt_day.findValue(TAG_FCTEXT).textValue();
//                    String icon = txt_day.findValue(TAG_ICON_URL).textValue();
//                    String dayName = txt_day.findValue(mTITLE).textValue();

                    String dayName = day.get(mDATE).findValue(mWEEKDAY).textValue();
                    String condition = day.findValue(TAG_CONDITIONS).textValue();
                    String icon = day.findValue(TAG_ICON_URL).textValue();
                    String high = day.get(TAG_HIGH).findValue(mFAHR).textValue();
                    String low = day.get(TAG_LOW).findValue(mFAHR).textValue();

                    HashMap<String, String> forecastInformation = new HashMap<>();
                    forecastInformation.put(TAG_DAY_NAME, dayName);
                    forecastInformation.put(TAG_HIGH, high + "f");
                    forecastInformation.put(TAG_LOW, low + "f");
                    forecastInformation.put(TAG_CONDITION, condition);
                    forecastInformation.put(TAG_ICON_URL, icon);
                    forecastInformationArrayList.add(forecastInformation);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        MyRecyclerAdapter forecastAdapter = new MyRecyclerAdapter(activity, forecastInformationArrayList, FORECAST);
        ForeCastInformationFragment.mRecyclerView.setAdapter(forecastAdapter);
    }

    /**
     * Set the url to use for hour intervals in the list view
     */
    public void setForeCastUrl(String latLong) {
        String FORMAT = "." + "json";

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("api.wunderground.com")
                .appendPath("api")
                .appendPath(WEATHER_ID)
                .appendPath(TAG_TEN_DAY_FORECAST)
                .appendPath("q")
                .appendPath(latLong + FORMAT);
        FORECAST_CONDITIONS_URL = builder.build().toString();
    }
}


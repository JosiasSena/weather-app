package josias.simple_weather.weather;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import josias.simple_weather.R;
import josias.simple_weather.adapters.MyRecyclerAdapter;
import josias.simple_weather.fragments.HourlyInformationFragment;

import static josias.simple_weather.activities.MainActivity.exitApplication;
import static josias.simple_weather.activities.MainActivity.hourlyInformationArrayList;
import static josias.simple_weather.activities.MainActivity.nowInfo;
import static josias.simple_weather.fragments.CurrentInformationFragment.conditionTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.dewpointTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.feelsLikeTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.humidityTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.locationTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.tempTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.todaysWeatherImageView;
import static josias.simple_weather.fragments.CurrentInformationFragment.windDirTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.windSpeedTV;
import static josias.simple_weather.fragments.CurrentInformationFragment.windchillTV;
import static josias.simple_weather.util.Constants.HOURLY;
import static josias.simple_weather.util.Constants.TAG_CONDITION;
import static josias.simple_weather.util.Constants.TAG_CURRENT_OBSERVATION;
import static josias.simple_weather.util.Constants.TAG_DEW_POINT;
import static josias.simple_weather.util.Constants.TAG_DEW_POINT_F;
import static josias.simple_weather.util.Constants.TAG_DISPLAY_LOCATION;
import static josias.simple_weather.util.Constants.TAG_ENGLISH;
import static josias.simple_weather.util.Constants.TAG_FCTTIME;
import static josias.simple_weather.util.Constants.TAG_FEELSLIKE;
import static josias.simple_weather.util.Constants.TAG_FEELS_LIKE_F;
import static josias.simple_weather.util.Constants.TAG_FORECAST;
import static josias.simple_weather.util.Constants.TAG_FULL;
import static josias.simple_weather.util.Constants.TAG_HOURLYROOT;
import static josias.simple_weather.util.Constants.TAG_HOURLY_TEMP;
import static josias.simple_weather.util.Constants.TAG_HUMIDITY;
import static josias.simple_weather.util.Constants.TAG_ICON_URL;
import static josias.simple_weather.util.Constants.TAG_LOCATION;
import static josias.simple_weather.util.Constants.TAG_MDAY;
import static josias.simple_weather.util.Constants.TAG_RELATIVE_HUMIDITY;
import static josias.simple_weather.util.Constants.TAG_TEMP_F;
import static josias.simple_weather.util.Constants.TAG_TIME;
import static josias.simple_weather.util.Constants.TAG_WEATHER;
import static josias.simple_weather.util.Constants.TAG_WIND_CHILL;
import static josias.simple_weather.util.Constants.TAG_WIND_CHILL_F;
import static josias.simple_weather.util.Constants.TAG_WIND_DIR;
import static josias.simple_weather.util.Constants.TAG_WIND_DIRECTION;
import static josias.simple_weather.util.Constants.TAG_WIND_SPEED;
import static josias.simple_weather.util.Constants.TAG_WIND_SPEED_MPH;
import static josias.simple_weather.util.Constants.WEATHER_ID;

/**
 * - Fetches all of our information from http://www.wunderground.com/ to display
 * - Initializes and sets an adapter to our hourlyForecastListView
 * - Sets the setOnItemClickListener to the hourlyForecastListView
 */
public class GetHourlyConditions extends AsyncTask<Void, Void, Void> {

    private ProgressDialog progressDialog;
    private HashMap<String, String> ourCurrentInformation;
    private static final String LOG = GetHourlyConditions.class.getSimpleName();

    private String CUR_CONDITIONS_URL;
    private String HOURLY_CONDITIONS_URL;
    private final Activity activity;

    public GetHourlyConditions(Activity activity, String URL) {
        this.activity = activity;
        setHourlyUrl(URL);
        setCurrentUrl(URL);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        ObjectMapper currentMapper = new ObjectMapper();
        ObjectMapper hourlyMapper = new ObjectMapper();
        ourCurrentInformation = new HashMap<>();
        hourlyInformationArrayList = new ArrayList<>();

        try {
            /**
             * #################### Current Information ####################
             */
            Log.d("CUR_CONDITIONS_URL", CUR_CONDITIONS_URL);
            JsonNode currentRootNode = currentMapper.readTree(new URL(CUR_CONDITIONS_URL));
            JsonNode mCurrentObs = currentRootNode.get(TAG_CURRENT_OBSERVATION);

            if (mCurrentObs == null) {
                showExitDialog();
                Log.e(LOG, activity.getString(R.string.null_root) + " --> " + activity.getString(R.string.exeeding_plan));
            } else {
                final JsonNode tempNode = currentRootNode;
                JsonNode displayLocation = mCurrentObs.get(TAG_DISPLAY_LOCATION);
                String full = displayLocation.findValue(TAG_FULL).textValue();
                String temp = mCurrentObs.get(TAG_TEMP_F).toString();
                String weather = mCurrentObs.get(TAG_WEATHER).toString();
                String cur_humidity = mCurrentObs.get(TAG_RELATIVE_HUMIDITY).toString();
                String cur_wind_dir = mCurrentObs.get(TAG_WIND_DIR).toString();
                String cur_wind_speed = mCurrentObs.get(TAG_WIND_SPEED_MPH).toString();
                String cur_dew_point_f = mCurrentObs.get(TAG_DEW_POINT_F).toString();
                String cur_wind_chill_f = mCurrentObs.get(TAG_WIND_CHILL_F).toString();
                String cur_feels_like_f = mCurrentObs.get(TAG_FEELS_LIKE_F).toString();

                // Add everything to our information hash map
                ourCurrentInformation.put(TAG_FULL, full);
                ourCurrentInformation.put(TAG_TEMP_F, temp + "f");
                ourCurrentInformation.put(TAG_WEATHER, weather.substring(1, weather.length() - 1));
                ourCurrentInformation.put(TAG_RELATIVE_HUMIDITY, activity.getResources().getString(R.string.humidity) +
                        cur_humidity.substring(1, cur_humidity.length() - 1));
                ourCurrentInformation.put(TAG_WIND_DIR, activity.getResources().getString(R.string.wind_dir) +
                        cur_wind_dir.substring(1, cur_wind_dir.length() - 1));
                ourCurrentInformation.put(TAG_WIND_SPEED_MPH, activity.getResources().getString(R.string.wind_speed) +
                        cur_wind_speed + "mph");
                ourCurrentInformation.put(TAG_DEW_POINT_F, activity.getResources().getString(R.string.dew_point) +
                        cur_dew_point_f + "f");

                if (!cur_wind_chill_f.equals("NA")) {
                    ourCurrentInformation.put(TAG_WIND_CHILL_F, activity.getResources().getString(R.string.wind_chill) +
                            cur_wind_chill_f.substring(1, cur_wind_chill_f.length() - 1) + "f");
                } else {
                    ourCurrentInformation.put(TAG_WIND_CHILL_F, "None");
                }

                ourCurrentInformation.put(TAG_FEELS_LIKE_F, activity.getResources().getString(R.string.feels_like) +
                        cur_feels_like_f.substring(1, cur_feels_like_f.length() - 1) + "f");
                ourCurrentInformation.put(TAG_ICON_URL, tempNode.get(TAG_FORECAST).findValue(TAG_ICON_URL).textValue());

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (todaysWeatherImageView != null) {
                            Picasso.with(activity.getApplicationContext())
                                    .load(tempNode.get(TAG_FORECAST).findValue(TAG_ICON_URL).textValue())
                                    .fit()
                                    .noFade()
                                    .into(todaysWeatherImageView);
                        }
                    }
                });

                /**
                 * #################### Hourly Information ####################
                 */

                // hourly conditions list view
                Log.i("HOURLY_CONDITIONS_URL", HOURLY_CONDITIONS_URL);
                JsonNode hourlyrootNode = hourlyMapper.readTree(new URL(HOURLY_CONDITIONS_URL));
                JsonNode hourlyRoot = hourlyrootNode.get(TAG_HOURLYROOT); // array with all days inside

                // Create a Calendar to get the year, month, and day
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                for (int i = 0; i < hourlyRoot.size(); i++) {
                    // Makes sure we only get hourly conditions for today only
                    JsonNode hour = hourlyRoot.get(i); // get the hour
                    int today = hour.get(TAG_FCTTIME).findValue(TAG_MDAY).asInt();

                    if (today == day) {
                        String time = hour.get(TAG_FCTTIME).findValue(TAG_TIME).textValue();
                        String temperature = hour.get(TAG_HOURLY_TEMP).findValue(TAG_ENGLISH).textValue();
                        String feelsLike = hour.get(TAG_FEELSLIKE).findValue(TAG_ENGLISH).textValue();
                        String condition = hour.findValue(TAG_CONDITION).textValue();

                        String icon = hour.findValue(TAG_ICON_URL).textValue();
                        String humidity = hour.findValue(TAG_HUMIDITY).textValue();
                        String dewPoint = hour.get(TAG_DEW_POINT).findValue(TAG_ENGLISH).textValue();
                        String windSpeed = hour.get(TAG_WIND_SPEED).findValue(TAG_ENGLISH).textValue();
                        String windChill = hour.get(TAG_WIND_CHILL).findValue(TAG_ENGLISH).textValue();
                        String windDirection = hour.get(TAG_WIND_DIRECTION).findValue("dir").textValue();

                        HashMap<String, String> hourlyInformation = new HashMap<>();
                        hourlyInformation.put(TAG_TIME, time);
                        hourlyInformation.put(TAG_HOURLY_TEMP, temperature + "f");
                        hourlyInformation.put(TAG_FEELSLIKE, activity.getString(R.string.feels_like) + " " + feelsLike + "f");
                        hourlyInformation.put(TAG_CONDITION, condition);
                        hourlyInformation.put(TAG_ICON_URL, icon);
                        hourlyInformation.put(TAG_HUMIDITY, humidity + "%");
                        hourlyInformation.put(TAG_DEW_POINT, dewPoint + "f");
                        hourlyInformation.put(TAG_WIND_SPEED, windSpeed + "mph");
                        hourlyInformation.put(TAG_WIND_CHILL, windChill);
                        hourlyInformation.put(TAG_WIND_DIRECTION, windDirection);

                        hourlyInformationArrayList.add(hourlyInformation);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        nowInfo = new HashMap<>();
        if (nowInfo.size() <= 0) {
            nowInfo.put(TAG_LOCATION, ourCurrentInformation.get(TAG_FULL));
            nowInfo.put(TAG_HOURLY_TEMP, ourCurrentInformation.get(TAG_TEMP_F));
            nowInfo.put(TAG_WEATHER, ourCurrentInformation.get(TAG_WEATHER));
            nowInfo.put(TAG_HUMIDITY, ourCurrentInformation.get(TAG_RELATIVE_HUMIDITY));
            nowInfo.put(TAG_WIND_DIRECTION, ourCurrentInformation.get(TAG_WIND_DIR));
            nowInfo.put(TAG_FEELSLIKE, ourCurrentInformation.get(TAG_FEELS_LIKE_F));
            nowInfo.put(TAG_WIND_SPEED, ourCurrentInformation.get(TAG_WIND_SPEED_MPH));
            nowInfo.put(TAG_WIND_CHILL, ourCurrentInformation.get(TAG_WIND_CHILL_F));
            nowInfo.put(TAG_DEW_POINT, ourCurrentInformation.get(TAG_DEW_POINT_F));
            nowInfo.put(TAG_ICON_URL, ourCurrentInformation.get(TAG_ICON_URL));
        }

        locationTV.setText(nowInfo.get(TAG_LOCATION));
        tempTV.setText(nowInfo.get(TAG_HOURLY_TEMP));
        conditionTV.setText(nowInfo.get(TAG_WEATHER));
        humidityTV.setText(nowInfo.get(TAG_HUMIDITY));
        windDirTV.setText(nowInfo.get(TAG_WIND_DIRECTION));
        feelsLikeTV.setText(nowInfo.get(TAG_FEELSLIKE));
        windSpeedTV.setText(nowInfo.get(TAG_WIND_SPEED));
        windchillTV.setText(nowInfo.get(TAG_WIND_CHILL));
        dewpointTV.setText(nowInfo.get(TAG_DEW_POINT));

        MyRecyclerAdapter hourlyAdapter = new MyRecyclerAdapter(activity, hourlyInformationArrayList, HOURLY);
        HourlyInformationFragment.mRecyclerView.setAdapter(hourlyAdapter);

//        WeatherNotification weatherNotification = new WeatherNotification(activity, onGoingNotification);
//        weatherNotification.display();
//        weatherNotification.enableNotification(enableNotifications);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // if build is Lollipop or higher then set notification visibility to public
//            weatherNotification.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showExitDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                alertDialog.setTitle(activity.getString(R.string.null_root));
                alertDialog.setMessage("'" + activity.getString(R.string.exeeding_plan) + "'");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(activity.getString(R.string.exit_app_menu),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                exitApplication();
                            }
                        });

                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });
    }

    /**
     * Set the url of our current location
     *
     * @param latLong
     */
    public void setCurrentUrl(String latLong) {
        String FORMAT = "." + "json";

        CUR_CONDITIONS_URL =
                "http://api.wunderground.com/api/" +
                        WEATHER_ID +
                        "/forecast/geolookup/conditions/q/" +
                        latLong + FORMAT;
    }

    /**
     * Set the url to use for hour intervals in the list view
     *
     * @param latLong
     */
    public void setHourlyUrl(String latLong) {
        String FORMAT = "." + "json";

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("api.wunderground.com")
                .appendPath("api")
                .appendPath(WEATHER_ID)
                .appendPath("hourly")
                .appendPath("q")
                .appendPath(latLong + FORMAT);
        HOURLY_CONDITIONS_URL = builder.build().toString();
    }
}

package josias.simple_weather.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;

import josias.simple_weather.R;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private PreferenceScreen preferenceScreen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        preferenceScreen = getPreferenceScreen();
        initSummary(preferenceScreen);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePrefSummary(findPreference(key));
    }

    /**
     * checks to see if we have a PreferenceGroup in our R.xml.settings
     *   - If we do go through it and check all types of preferences inside of it
     *   and initialize their summaries
     *   - if we don't then just updatePrefSummary
     */
    private void initSummary(Preference preference) {
        if (preference instanceof PreferenceGroup) {
            PreferenceGroup preferenceGroup = (PreferenceGroup) preference;
            for (int i = 0; i < preferenceGroup.getPreferenceCount(); i++) {
                initSummary(preferenceGroup.getPreference(i));
            }
        } else {
            updatePrefSummary(preference);
        }
    }

    /**
     * Check for all types of preference ahead of time for future purposes and update each
     * preference summary accordingly.
     */
    private void updatePrefSummary(Preference preference) {
        // Set the summary to the selected option
        if (preference instanceof ListPreference) {
            ListPreference listPref = (ListPreference) preference;
            preference.setSummary(listPref.getEntry());
        }

        // Set the summary as the text that was entered in the text field
        if (preference instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) preference;
            preference.setSummary(editTextPref.getText());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register listener to check for changed preference
        preferenceScreen.getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener
        preferenceScreen.getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}

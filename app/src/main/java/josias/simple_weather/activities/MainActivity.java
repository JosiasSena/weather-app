package josias.simple_weather.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import josias.simple_weather.R;
import josias.simple_weather.adapters.PagerAdapter;
import josias.simple_weather.util.LoadAllWeatherInformation;
import josias.simple_weather.util.databases.MyLocationsDatabaseHelper;
import josias.simple_weather.weather.GetForecastConditions;
import josias.simple_weather.weather.GetHourlyConditions;

import static josias.simple_weather.util.Constants.REQUEST_RESOLVE_ERROR;

public class MainActivity extends ActionBarActivity implements ActionBar.OnNavigationListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final String TAG = "MainActivity";

    private static double mLONGITUDE, mLATITUDE;
    private String passedLatLong, currentLatLong;
    private boolean leaveApp = false;

    private static GetHourlyConditions getHourlyConditions;
    private static GetForecastConditions getForecastConditions;

    private MyLocationsDatabaseHelper mHelper;
    private static ActionBar actionBar;
    public static ArrayAdapter<String> actionBarAdapter;
    public static List<josias.simple_weather.objects.Location> mLocationsArray;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public static HashMap<String, String> nowInfo;
    public static ArrayList<HashMap<String, String>> hourlyInformationArrayList;
    public static ArrayList<HashMap<String, String>> forecastInformationArrayList;
    public static boolean onGoingNotification, enableNotifications;

    private static final String mGOOGLE_CLIENT_LOG = "GoogleApiClient error";
    private static final String mGOOGLE_CLIENT_NULL = "GoogleApiClient is null";
    private static final String mREFRESH_INTERVAL = "refreshInterval";

    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupAd();
        setUpActionBar();
        initTabs();

        // Generate SHA1 for google API usage
        // GenerateAppSHA1 generateAppSHA1 = new GenerateAppSHA1(this);
        // Log.i("SHA1", generateAppSHA1.getCertificateSHA1Fingerprint());

        // Before we do anything, lets make sure there is internet connectivity.
        if (!isNetworkAvailable()) {
            // If there is no network then show a dialog with no network message
            noNetworkDialog();
        } else {
            getCurrentLocation();
        }

        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    private void setupAd() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-4518042852936654/4912926327");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.d(TAG, "onAdFailedToLoad() called with: " + "errorCode = [" + errorCode + "]");
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.d(TAG, "onAdLeftApplication() called");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.d(TAG, "onAdOpened() called");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d(TAG, "onAdLoaded() called");
            }

            @Override
            public void onAdClosed() {
                super.onAdLoaded();
                Log.d(TAG, "onAdClosed() called");
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("F58C7B23EDD8A1ECE5C53853E63627AA")
                .build();

        interstitialAd.loadAd(adRequest);
    }

    /**
     * Updates weather information every SETTINGS_REFRESH_INTERVAL minutes
     */
    private void setSettings() {
        SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        int SETTINGS_REFRESH_INTERVAL = Integer.parseInt(setting.getString(mREFRESH_INTERVAL, "3600000"));
        onGoingNotification = setting.getBoolean("onGoingSetting", true);
        enableNotifications = setting.getBoolean("enableNotifications", true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initConditions(currentLatLong);
                getWeatherInfo();
            }
        }, SETTINGS_REFRESH_INTERVAL);
    }

    /**
     * Setup the action bar
     * Setup actionbar dropdown
     * Setup dropdown items/locations
     */
    private void setUpActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // if build is Lollipop or higher then change the phone status bar color to orange
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setLogo(R.drawable.actionbar_icon2);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        loadActionBarLocations();
    }

    /**
     * Create array of items for the locations in the actionbar dropdown
     */
    private void loadActionBarLocations() {
        // Initialize our locations DBS helper
        mHelper = new MyLocationsDatabaseHelper(this);

        // Fetch all of our locations from the SQLite DBS
        mLocationsArray = mHelper.getAllLocations();

        ArrayList<String> mPlaces = new ArrayList<>();
        mPlaces.add(getString(R.string.my_loc));
        for (int i = 0; i < mLocationsArray.size(); i++) {
            mPlaces.add(i + 1, mLocationsArray.get(i).getName());
        }

        actionBarAdapter = new ArrayAdapter<>(this, R.layout.simple_list_item, mPlaces);
        actionBar.setListNavigationCallbacks(actionBarAdapter, this);
    }

    /**
     * Initializes variables and loads weather information for the current day
     */
    private void init() {
        // Initiate all Urls
        String latLong = mLATITUDE + "," + mLONGITUDE;
        currentLatLong = latLong;
        initConditions(latLong);

        LoadAllWeatherInformation loadAllWeatherInformation = new LoadAllWeatherInformation(this,
                getHourlyConditions, getForecastConditions);
        loadAllWeatherInformation.load();
    }

    private void newInfoInit() {
        currentLatLong = mLATITUDE + "," + mLONGITUDE;
        initConditions(currentLatLong);
        getWeatherInfo();
    }

    /**
     * initialize the tabs
     */
    private void initTabs() {
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(pagerAdapter.getCount());
        viewPager.setAdapter(pagerAdapter);

        PagerSlidingTabStrip pagerTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pagerTabStrip.setShouldExpand(true);
        pagerTabStrip.setAllCaps(true);
        pagerTabStrip.setTextColor(getResources().getColor(android.R.color.holo_orange_dark));
        pagerTabStrip.setIndicatorHeight(10);
        pagerTabStrip.setIndicatorColor(Color.parseColor("#8bc34a"));
        pagerTabStrip.setViewPager(viewPager);
    }

    /**
     * Initiate GetConditions AsyncTask
     */
    private void initConditions(String URL) {
        getHourlyConditions = new GetHourlyConditions(this, URL);
        getForecastConditions = new GetForecastConditions(this, URL);
    }

    private static void getWeatherInfo() {
        getHourlyConditions.execute();
        getForecastConditions.execute();
    }

    /**
     * Gets our current information in Latitude anf Longitude format
     * Checks if the GPS is enabled before it gets our Latitude anf Longitude
     * - If the GPS is not enabled, the method sends us to the settings in our
     * mobile device to enable the GPS
     */
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean is_gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!is_gps_enabled) {
            // if we do not have GPS enabled to the following
            final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(R.string.enable_gps);
            dialog.setCancelable(false); // Force to click enable
            dialog.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.show();
        } else {
            if (mGoogleApiClient != null) {
                if (mGoogleApiClient.isConnected()) {
                    newInfoInit();
                } else {
                    Log.e("Connection error", "Not connected");
                }
            } else {
                buildGoogleApiClient();
                createLocationRequest();
            }
        }
    }

    /**
     * Receives latitude and longitude of new location from MyLocationsActivity and
     * loads all of the information for the new location
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR && resultCode == RESULT_OK) {
            // Make sure the app is not already connected or attempting to connect
            if (!mGoogleApiClient.isConnecting() &&
                    !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Load/refresh the information of the other location we are interested on, rather then our current one.
     * If we are currently looking at our current location's information then just refresh it
     */
    private void getOtherLocation() {
        if (passedLatLong != null) {
            if (passedLatLong.equals(String.valueOf(mLATITUDE + mLONGITUDE))) {
                getCurrentLocation();
            }

            initConditions(passedLatLong);
            getWeatherInfo();
        } else {
            getCurrentLocation();
        }
    }

    /**
     * Check for network connectivity
     *
     * @return true/false
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void noNetworkDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.no_nework));
        alertDialog.setMessage(getString(R.string.connetc_to_net));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.exit_app_menu),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        exitApplication();
                    }
                });

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent menuIntent;
        if (item.getItemId() == R.id.myLocations) {
            menuIntent = new Intent(this, MyLocationsListActivityDialog.class);
            startActivityForResult(menuIntent, 0);
        } else {
            menuIntent = new Intent(this, SettingsActivity.class);
            startActivity(menuIntent);
        }

        return true;
    }

    /**
     * Does something depending on which item is clicked in the actionbar dropdown menu
     */
    @Override
    public boolean onNavigationItemSelected(int position, long id) {

        if (position == 0) {
            getCurrentLocation();
        } else {
            passedLatLong = mLocationsArray.get(position - 1).getLatLong();
            getOtherLocation();
        }
        return false;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mLONGITUDE = mLastLocation.getLongitude();
            mLATITUDE = mLastLocation.getLatitude();
            init();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("onConnectionFailed", "Connection to google API client failed.");
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Build the GoogleAPIClient for Google API usage
     * This is mandatory for all Google APIs
     */
    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLONGITUDE = location.getLongitude();
        mLATITUDE = location.getLatitude();
        init();
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(60 * 1000)        // 1 minute, in milliseconds
                .setFastestInterval(30000); // 30 seconds, in milliseconds
    }

    /**
     * Completely closes the application
     * Note: i know it is recommended to just use finish();
     * but i just wanted to show this method as well
     */
    public static void exitApplication() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    @Override
    public void onBackPressed() {
        if (leaveApp) {
            exitApplication();
        } else {
            // if false
            Toast.makeText(this, R.string.exit_app, Toast.LENGTH_SHORT).show();
            leaveApp = true; // turn exit to true so that we can exit

            // Wait 3 seconds, if the back button is pressed again within 3 seconds then we exit the app
            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    leaveApp = false;
                }
            }, 3000);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            Log.e(mGOOGLE_CLIENT_LOG, mGOOGLE_CLIENT_NULL);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.getWritableDatabase();
        setSettings();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        } else {
            Log.e(mGOOGLE_CLIENT_LOG, mGOOGLE_CLIENT_NULL);
        }

        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.close();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
            }
        } else {
            Log.e(mGOOGLE_CLIENT_LOG, mGOOGLE_CLIENT_NULL);
        }
    }
}

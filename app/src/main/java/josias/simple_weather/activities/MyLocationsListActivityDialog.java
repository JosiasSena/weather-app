package josias.simple_weather.activities;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

import josias.simple_weather.R;
import josias.simple_weather.adapters.LocationsListAdapter;
import josias.simple_weather.objects.Location;
import josias.simple_weather.util.SearchForLocations;
import josias.simple_weather.util.databases.MyLocationsDatabaseHelper;

import static josias.simple_weather.activities.MainActivity.actionBarAdapter;
import static josias.simple_weather.activities.MainActivity.mLocationsArray;
import static josias.simple_weather.util.Constants.BAD_COORDINATES;
import static josias.simple_weather.util.Constants.TAG_COORDINATES;
import static josias.simple_weather.util.Constants.TAG_NAME;

public class MyLocationsListActivityDialog extends ListActivity {

    public static SearchView searchView;
    private MyLocationsDatabaseHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_locations);
        init();
    }

    private void init() {
        initList();
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchQueryTextListener());
        searchView.setOnSuggestionListener(new SearchQuerySuggestionListener());
    }

    /**
     * Initialize our listView
     */
    private void initList() {
        // Get our listView
        ListView mList = getListView();

        // Initialize our locations DBS helper
        mHelper = new MyLocationsDatabaseHelper(this);

        // Fetch all of our locations from the SQLite DBS
        mLocationsArray = mHelper.getAllLocations();

        LocationsListAdapter adapter = new LocationsListAdapter(getApplicationContext(), mLocationsArray, mHelper);
        mList.setAdapter(adapter);
    }

    /**
     * Get all of our data to populate our searchView with
     * @param query the query to use for looking for locations
     */
    private void loadData(String query) {
        new SearchForLocations(this, query).execute();
    }

    /**
     * Returns suggestions as we type inside the searchView
     */
    private class SearchQueryTextListener implements SearchView.OnQueryTextListener {
        @Override
        public boolean onQueryTextSubmit(String query) {
            // Do this when we press enter or submit
            loadData(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String query) {
            // Do this as we type in the search view
            loadData(query);
            return false;
        }
    }

    /**
     * Tells us what happens when we click on one of the suggestions
     */
    private class SearchQuerySuggestionListener implements SearchView.OnSuggestionListener {
        @Override
        public boolean onSuggestionSelect(int position) {
            return false;
        }

        @Override
        public boolean onSuggestionClick(int position) {
            // what do we do when we click on the suggestion shown?
            CursorAdapter cAdapter = searchView.getSuggestionsAdapter();
            Cursor cursor = cAdapter.getCursor();
            cursor.moveToPosition(position);

            String city_name = cursor.getString(cursor.getColumnIndex(TAG_NAME));
            String latLong = cursor.getString(cursor.getColumnIndex(TAG_COORDINATES));

            if (latLong.equals(BAD_COORDINATES)) {
                Toast.makeText(getApplicationContext(), getString(R.string.cant_choose_country), Toast.LENGTH_SHORT).show();
            } else {
                Location location = new Location();
                location.setName(city_name);
                location.setLatLong(latLong);

                mHelper.insertNewLocation(location);
                initList();
                updateActionBarAdapter();
            }

            return true;
        }
    }

    /**
     * Updates the dropdown menu for the MainActivity actionbar
     */
    private void updateActionBarAdapter(){
        ArrayList<String> mPlaces = new ArrayList<>();
        mPlaces.add(getString(R.string.my_loc));
        for (int i = 0; i < mLocationsArray.size(); i++) {
            mPlaces.add(i + 1, mLocationsArray.get(i).getName());
        }

        actionBarAdapter.clear();
        actionBarAdapter.addAll(mPlaces);
        actionBarAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        mHelper.getWritableDatabase();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mHelper.close();
        super.onPause();
    }
}

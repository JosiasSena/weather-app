package josias.simple_weather.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import josias.simple_weather.R;
import josias.simple_weather.objects.Hour;

import static josias.simple_weather.util.Constants.HOUR_KEY;

public class MoreItemInfoActivity extends ActionBarActivity {
    private String EMAIL_MESSAGE;
    private String EMAIL_SUBJECT;

    @InjectView(R.id.extraTempTV) TextView extraTempTV;
    @InjectView(R.id.extraHumidityTV) TextView extraHumidityTV;
    @InjectView(R.id.extraDewPointTV) TextView extraDewPointTV;
    @InjectView(R.id.extraWindSpeedTV) TextView extraWindSpeedTV;
    @InjectView(R.id.extraWindChillTV) TextView extraWindChillTV;
    @InjectView(R.id.extraWindDirTV) TextView extraWindDirTV;
    @InjectView(R.id.extraConditionTV) TextView extraConditionTV;
    @InjectView(R.id.moreInfoFeelsLikeTV) TextView moreInfoFeelsLikeTV;
    @InjectView(R.id.weatherIcon) ImageView weatherIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_item_info);
        ButterKnife.inject(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // if build is Lollipop or higher then change the phone status bar color to orange
            getWindow().setStatusBarColor(getResources().getColor(android.R.color.holo_orange_dark));
        }

        init();
    }

    private void init() {
        Hour mHour = getIntent().getParcelableExtra(HOUR_KEY);

        Picasso.with(this).load(mHour.getICON_URL()).fit().noFade().into(weatherIcon);

        getSupportActionBar().setTitle(mHour.getTIME());
        extraTempTV.setText(mHour.getTEMP());
        extraHumidityTV.setText(getString(R.string.humidity) + mHour.getHUMIDITY());
        extraDewPointTV.setText(getString(R.string.dew_point) + mHour.getDEW_POINT());
        extraWindSpeedTV.setText(getString(R.string.wind_speed) + mHour.getWIND_SPEED());
        extraWindChillTV.setText(getString(R.string.wind_chill) + mHour.getWIND_CHILL());
        extraWindDirTV.setText(getString(R.string.wind_dir) + mHour.getWIND_DIRECTION());
        moreInfoFeelsLikeTV.setText(mHour.getFEELS_LIKE());
        extraConditionTV.setText(mHour.getCONDITION());

        EMAIL_SUBJECT = getString(R.string.weather_info) + mHour.getTIME();
        EMAIL_MESSAGE = extraHumidityTV.getText() + "\n" +
                extraDewPointTV.getText() + "\n" +
                extraWindSpeedTV.getText() + "\n" +
                extraWindChillTV.getText() + "\n" +
                extraWindDirTV.getText() + "\n";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_more_item_info, menu);
        MenuItem shareMenuItem = menu.findItem(R.id.share);

        ShareActionProvider mShareActionProvider = new ShareActionProvider(this);
        MenuItemCompat.setActionProvider(shareMenuItem, mShareActionProvider);

        Intent shareIntent = getDefaultShareIntent();
        if (shareIntent != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }

        return true;
    }

    /**
     * Returns a share intent
     */
    private Intent getDefaultShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, EMAIL_SUBJECT);
        intent.putExtra(Intent.EXTRA_TEXT, EMAIL_MESSAGE);
        return intent;
    }
}

package josias.simple_weather.widget;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import josias.simple_weather.R;

public class UpdateWidgetService extends Service implements LocationListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String LOG = UpdateWidgetService.class.getSimpleName();
    private double mLONGITUDE, mLATITUDE;
    public static RemoteViews views;
    public static AppWidgetManager appWidgetManager;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    static String latLong;
    private int[] ALL_WIDGET_IDS;
    private int WIDGET_ID;

    public UpdateWidgetService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException(getString(R.string.not_implemented));
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
        int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        for (int widgetId : allWidgetIds) {
            WIDGET_ID = widgetId;
            ALL_WIDGET_IDS = allWidgetIds;

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, MMMM dd");
            simpleDateFormat.setCalendar(GregorianCalendar.getInstance());
            String dateFormatted = simpleDateFormat.format(GregorianCalendar.getInstance().getTime());

            views = new RemoteViews(getApplicationContext().getPackageName(), R.layout.app_widget);
            views.setTextViewText(R.id.widgetDate, dateFormatted);

            getCurrentLocation();
        }
        stopSelf();

        return super.onStartCommand(intent, PendingIntent.FLAG_UPDATE_CURRENT, startId);
    }

    static void loadImage(final RemoteViews views, final String icon_url) {
        Thread mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (icon_url != null) {
                        URL url = new URL(icon_url);
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        views.setImageViewBitmap(R.id.widgetImage, image);
                    } else {
                        Log.e(LOG, "loadImage URL is null");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mThread.start();
        try {
            mThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets our current information in Latitude anf Longitude format
     */
    private void getCurrentLocation() {
        buildGoogleApiClient();
        createLocationRequest();
    }

    /**
     * Called when a new location is found by the network location provider.
     *
     * @param location my current location
     */
    @Override
    public void onLocationChanged(Location location) {
        mLONGITUDE = location.getLongitude();
        mLATITUDE = location.getLatitude();
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(3600 * 1000)        // 1 hour, in milliseconds
                .setFastestInterval(600000); // 10 minutes, in milliseconds
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            startLocationUpdates();

            mLONGITUDE = mLastLocation.getLongitude();
            mLATITUDE = mLastLocation.getLatitude();
            latLong = mLATITUDE + "," + mLONGITUDE;

            new GetWidgetWeatherInformation(getApplicationContext(), WIDGET_ID, ALL_WIDGET_IDS).execute();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("onConnectionFailed", "Connection to google API client failed.");
    }
}

package josias.simple_weather.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;

import josias.simple_weather.R;
import josias.simple_weather.activities.MainActivity;

import static josias.simple_weather.util.Constants.TAG_CURRENT_OBSERVATION;
import static josias.simple_weather.util.Constants.TAG_DISPLAY_LOCATION;
import static josias.simple_weather.util.Constants.TAG_FORECAST;
import static josias.simple_weather.util.Constants.TAG_FULL;
import static josias.simple_weather.util.Constants.TAG_ICON_URL;
import static josias.simple_weather.util.Constants.TAG_TEMP;
import static josias.simple_weather.util.Constants.TAG_ZIP;
import static josias.simple_weather.util.Constants.WEATHER_ID;

class GetWidgetWeatherInformation extends AsyncTask<Void, Void, Void> {
    private static final String LOG = GetWidgetWeatherInformation.class.getSimpleName();
    private static final String FORMAT = "." + "json";

    private final Context context;
    private final int widgetId;
    private final int[] allWidgetIds;
    private static String location;
    private static String weather;
    private static String icon_url;

    public GetWidgetWeatherInformation(Context context, int widgetId, int[] allWidgetIds) {
        this.context = context;
        this.widgetId = widgetId;
        this.allWidgetIds = allWidgetIds;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ObjectMapper widgetMapper = new ObjectMapper();

        String WIDGET_URL = "http://api.wunderground.com/api/" + WEATHER_ID +
                "/forecast/geolookup/conditions/q/" +
                UpdateWidgetService.latLong + FORMAT;

        try {
            Log.i("WIDGET_URL", WIDGET_URL);
            JsonNode currentRootNode = widgetMapper.readTree(new URL(WIDGET_URL));
            JsonNode mCurrentObs = currentRootNode.get(TAG_CURRENT_OBSERVATION);

            if (mCurrentObs == null) {
                Log.e(LOG, context.getString(R.string.null_root) + " --> " + context.getString(R.string.exeeding_plan));
            } else {
                JsonNode displayLocation = mCurrentObs.get(TAG_DISPLAY_LOCATION);

                String full = displayLocation.findValue(TAG_FULL).textValue();
                String zip = displayLocation.findValue(TAG_ZIP).textValue();

                JsonNode temp = mCurrentObs.get(TAG_TEMP);
                location = full + " " + zip;
                weather = String.valueOf(temp).substring(1, String.valueOf(temp).length() - 1);
                icon_url = currentRootNode.get(TAG_FORECAST).findValue(TAG_ICON_URL).textValue();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        UpdateWidgetService.views.setTextViewText(R.id.widgetLocation, location);
        UpdateWidgetService.views.setTextViewText(R.id.widgetWeather, weather);
        UpdateWidgetService.loadImage(UpdateWidgetService.views, icon_url);

        /**
         * Set onClickListeners
         */
        Intent clickIntent = new Intent(context, MainActivity.class);
        clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, clickIntent, 0);
        UpdateWidgetService.views.setOnClickPendingIntent(R.id.widgetMain, pendingIntent);

        UpdateWidgetService.appWidgetManager.updateAppWidget(widgetId, UpdateWidgetService.views);
    }
}
package josias.simple_weather;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * File created by josias on 11/28/15.
 */
public class App extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}

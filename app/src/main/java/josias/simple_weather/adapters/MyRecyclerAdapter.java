package josias.simple_weather.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import josias.simple_weather.R;
import josias.simple_weather.activities.MoreItemInfoActivity;
import josias.simple_weather.objects.Hour;
import josias.simple_weather.util.ViewHolder;

import static josias.simple_weather.util.Constants.FORECAST;
import static josias.simple_weather.util.Constants.HOURLY;
import static josias.simple_weather.util.Constants.HOUR_KEY;
import static josias.simple_weather.util.Constants.TAG_CONDITION;
import static josias.simple_weather.util.Constants.TAG_DAY_NAME;
import static josias.simple_weather.util.Constants.TAG_DEW_POINT;
import static josias.simple_weather.util.Constants.TAG_FEELSLIKE;
import static josias.simple_weather.util.Constants.TAG_HIGH;
import static josias.simple_weather.util.Constants.TAG_HOURLY_TEMP;
import static josias.simple_weather.util.Constants.TAG_HUMIDITY;
import static josias.simple_weather.util.Constants.TAG_ICON_URL;
import static josias.simple_weather.util.Constants.TAG_LOW;
import static josias.simple_weather.util.Constants.TAG_TIME;
import static josias.simple_weather.util.Constants.TAG_WIND_CHILL;
import static josias.simple_weather.util.Constants.TAG_WIND_DIRECTION;
import static josias.simple_weather.util.Constants.TAG_WIND_SPEED;

public class MyRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    private final Activity activity;
    private final ArrayList<HashMap<String, String>> informationArrayList;
    private int weatherType = HOURLY;

    public MyRecyclerAdapter(Activity activity, ArrayList<HashMap<String, String>> informationArrayList, int weatherType) {
        this.activity = activity;
        this.informationArrayList = informationArrayList;
        this.weatherType = weatherType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.hourly_listview_item, viewGroup, false);

        if (weatherType == FORECAST){
            view = LayoutInflater.from(activity).inflate(R.layout.forecast_listview_item, viewGroup, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        HashMap<String, String> weatherItem = informationArrayList.get(position);
        String url = weatherItem.get(TAG_ICON_URL);
        Picasso.with(activity).load(url).fit().noFade().into(viewHolder.mWeatherIcon);
        viewHolder.hourlyCondition.setText(weatherItem.get(TAG_CONDITION));

        if (weatherType == HOURLY){
            viewHolder.hourTime.setText(weatherItem.get(TAG_TIME));
            viewHolder.hourlyTemp.setText(weatherItem.get(TAG_HOURLY_TEMP));
            viewHolder.hourlyFeelsLike.setText(weatherItem.get(TAG_FEELSLIKE));
            viewHolder.itemView.setOnClickListener(new ItemViewOnClickListener(position));
        }

        if (weatherType == FORECAST){
            viewHolder.dayName.setText(weatherItem.get(TAG_DAY_NAME));
            viewHolder.hiTemp.setText(weatherItem.get(TAG_HIGH));
            viewHolder.loTemp.setText(weatherItem.get(TAG_LOW));
        }
    }

    @Override
    public int getItemCount() {
        return (null != informationArrayList ? informationArrayList.size() : 0);
    }

    /**
     * OnClickListener for the RecycleView items
     */
    private class ItemViewOnClickListener implements View.OnClickListener {
        private final int position;

        public ItemViewOnClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            Hour mHour = new Hour();
            mHour.setTIME(informationArrayList.get(position).get(TAG_TIME));
            mHour.setTEMP(informationArrayList.get(position).get(TAG_HOURLY_TEMP));
            mHour.setCONDITION(informationArrayList.get(position).get(TAG_CONDITION));
            mHour.setICON_URL(informationArrayList.get(position).get(TAG_ICON_URL));
            mHour.setHUMIDITY(informationArrayList.get(position).get(TAG_HUMIDITY));
            mHour.setDEW_POINT(informationArrayList.get(position).get(TAG_DEW_POINT));
            mHour.setWIND_SPEED(informationArrayList.get(position).get(TAG_WIND_SPEED));
            mHour.setWIND_CHILL(informationArrayList.get(position).get(TAG_WIND_CHILL));
            mHour.setWIND_DIRECTION(informationArrayList.get(position).get(TAG_WIND_DIRECTION));
            mHour.setFEELS_LIKE(informationArrayList.get(position).get(TAG_FEELSLIKE));

            Bundle mBundle = new Bundle();
            mBundle.putParcelable(HOUR_KEY, mHour);

            Intent moreInfoIntent = new Intent(activity, MoreItemInfoActivity.class);
            moreInfoIntent.putExtras(mBundle);
            activity.startActivity(moreInfoIntent);
        }
    }
}

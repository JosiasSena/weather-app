package josias.simple_weather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import josias.simple_weather.activities.MainActivity;
import josias.simple_weather.R;
import josias.simple_weather.objects.Location;
import josias.simple_weather.util.databases.MyLocationsDatabaseHelper;

public class LocationsListAdapter extends ArrayAdapter<Location> {
    private final MyLocationsDatabaseHelper mHelper;

    public LocationsListAdapter(Context applicationContext, List<Location> locations, MyLocationsDatabaseHelper mHelper) {
        super(applicationContext, 0, locations);
        this.mHelper = mHelper;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Location location = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.locations_listview_item, parent, false);
        }

        TextView locationName = (TextView) convertView.findViewById(R.id.locationName);
        locationName.setText(location.getName());

        ImageButton deleteLocation = (ImageButton) convertView.findViewById(R.id.deleteLocation);
        deleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelper.deleteLocation(location);
                remove(location);
                notifyDataSetChanged();

                MainActivity.actionBarAdapter.remove(location.getName());
                MainActivity.actionBarAdapter.notifyDataSetChanged();
            }
        });

        return convertView;
    }
}

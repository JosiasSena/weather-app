package josias.simple_weather.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import josias.simple_weather.fragments.CurrentInformationFragment;
import josias.simple_weather.fragments.ForeCastInformationFragment;
import josias.simple_weather.fragments.HourlyInformationFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(android.support.v4.app.FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        if (index == 0) {
            return new CurrentInformationFragment();
        } else if (index == 1) {
            return new HourlyInformationFragment();
        } else if (index == 2) {
            return new ForeCastInformationFragment();
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return "Now";
        } else if (position == 1) {
            return "Hourly";
        } else if (position == 2) {
            return "Forecast";
        }

        return null;
    }

    /**
     * @return returns total number of tabs
     */
    @Override
    public int getCount() {
        return 3;
    }
}

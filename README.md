#The Carrot Android Developer Challenge!

Here at carrot, we think a little differently about hiring. Rather than quizzing you on a whiteboard or drilling you on the spot with sysops questions, we'd prefer to see how you work in a natural and collaborative environment, similar to how you would be working were you to come on at Carrot. The interview is not so much about your technical skills as it is about judging whether you'd be a good culture fit here. This developer challenge is where we evaluate technical skills and how you work.

We are asking you to take the lead on a very simple app. It will be a weather app, in which a user can:

- Enter + store locations  
- Switch between locations easily  
- View the current conditions  
- View a summary of the hourly 1 day forecast in a list  
- View further details of hourly details  
- Refresh the weather addAccountData

You should use http://www.wunderground.com/ as your API

Along the course of the project, Carrot's developers will be checking out your progress and sometimes helping out with small things, making suggestions, complimenting you on a clever solution, or specifying a requirement to be added to the app (to see how flexibly you have written your code). This is how we work in real life: often times you will be working alongside other developers, and it is the reality with client work that new requirements can be added after the project has already begun. During this process, you can and should treat us as co-workers. If there's something you want to discuss, grab someone to talk it through with you. If you are short on time and need help with a feature, see if you can get someone to contribute. If you'd like review on a specific piece, ask for it.

We expect you to use Github for the development of this project. We use Github heavily here at Carrot and it's important that you are familiar and comfortable with it. We will create a private repository for you to do your development in and invite you to it. We would also ask that you use [the github workflow](https://guides.github.com/introduction/flow/index.html) with feature branches and pull requests, to make sure that all code is well-thought-out and easy for us to review. In general, this means laying down the foundation directly on master, but adding all other features through pull requests. This way, the code review can happen inside the pull request, and only approved code will go through to master.

You are welcome to use whatever libraries you feel most comforterable with, or would fit the problem the best.  At Carrot, we do have an extreme preference to use [Android Studio](https://developer.android.com/sdk/index.html) over Eclipse, as it is now at v1.0 and is The official Android IDE.  If you end up using Eclipse because that's what you currently have set up and you have time constraints, we won't deduct points (but you should really check out Android Studio -- it's quite nice).  Feel free to open an issue early on to discuss your choice of libraries, as picking the right tools can save you a bunch of time.

It's much more important to us to see you lay down clean and organized code and collaborate with us well than to go overboard with extra features and fancy things. While you are welcome to go outside the box if you'd personally enjoy this, all we are asking is for a straightforward and solid implementation of a small and simple Android app.

You can work on this project at your leisure, and use any resources you want to do so (other than getting someone else to do it for you, of course). Work as you normally would. We generally expect that this will be finished up in two weeks or less, but of course we understand that often times life can get busy, and we can be flexible if you have a special circumstance you discuss with us.

Good luck! To inspire you to totally nail this one, here is a picture of a fluffy puppy:

![fluffy puppy](http://cl.ly/XvJZ/puppy-fluffy.jpg)
